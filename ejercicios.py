import random
import math

class Resolve(object):
    def aleatorio(self, num):
        return random.randrange(num)

    def par(self, num):
        return num % 2 == 0

    def celcios(self, fahr):
        return round(5.0/9.0 * (fahr - 32), 2)

    def pow(self, base, exp):
        res = 1
        for i in range(exp):
            res *= base
        return res

    def hilera(self, hil, ancho):
        res = ''
        if len(hil) > ancho:
            res = 'la hilera es mayor que el tamanio del parrafo'
        else:
            total = ancho - len(hil)
            for i in range(total):
                res += '%s*' % (hil) if (round(total/2, 0)) == i else '*'
        return res

    # PEP 8
    def bubbleSort(self, elementos, ascendente):
        for i in range(1, len(elementos)):
            for j in range(0, len(elementos) - i):
                if ascendente:                    
                    if elementos[j] > elementos[j + 1]:
                        aux = elementos[j]
                        elementos[j] = elementos[j + 1]
                        elementos[j + 1] = aux
                else:
                    if elementos[j] > elementos[j + 1]:
                        aux = elementos[j]
                        elementos[j] = elementos[j + 1]
                        elementos[j + 1] = aux
        return elementos

    def multiplos(self, elementos):
        res = []
        for i in elementos:
            if (i % 4 == 0 or i % 7 == 0) and i < 1000:
                res.append(i)
        return self.bubbleSort(res, True)

    def piramide(self, alto):
        res = ''
        sobra = alto - 1
        for i in range(alto):
            intercalar = True
            for a in range(alto * 2):
                if a < sobra or a > (alto * 2 - 2) - sobra:
                    res += ' '
                else:
                    res += '*' if intercalar else ' '
                    intercalar = not intercalar
            res += '\n'
            sobra -= 1
        return res

    def tripletaPitagorica(self, num1, num2, num3):
        return (round(num1 ** 2 + num2 ** 2, 0) == round(num3 ** 2, 0)) or (round(num1 ** 2 + num3 ** 2, 0) == round(num2 ** 2, 0)) or (round(num3 ** 2 + num2 ** 2, 0) == round(num1 ** 2, 0))
    def palindromo(self, palabra):
        medio = len(palabra) / 2 if len(palabra)%2 == 0 else (len(palabra) - 1) / 2
        print medio
        print palabra[:medio]
        print palabra[-medio:]
        return palabra[:medio] == palabra[-medio:][::-1]

    def leerNumero(self, num):
        if int(num) <= 1000:
            numeros = list(num)
            res = ''
            unidades = ['', 'one', 'two', 'three', 'four', 'five', 'six', 'seven', 'eight', 'nine']
            decenas = ['', 'ten', 'twenty', 'thirty', 'forty', 'fifty', 'sixty', 'seventy', 'eighty', 'ninety']
            pos = 0
            tamanio = len(numeros)
            for i in range(len(numeros)):
                numeros[i] = int(numeros[i])

            if tamanio == 4:
                res += '%s %s ' % (unidades[numeros[pos]], 'thousand' if unidades[numeros[pos]] != 0 else '')
                pos += 1
                tamanio -= 1

            if tamanio == 3:
                res += '%s hundred ' % (unidades[numeros[pos]])
                pos += 1
                tamanio -= 1

            if tamanio == 2:
                res += '%s ' % (decenas[numeros[pos]])
                pos += 1
                tamanio -= 1
            
            if tamanio == 1:
                res += unidades[numeros[pos]]

            print res

                
            

    def validacionTriangulo(self, p1, p2, p3):
        puntos = []
        distancias = []
        puntos.append(p1)
        puntos.append(p2)
        puntos.append(p3)
        res = ''

        for p in range(len(puntos)):
            comparcion = p + 1 if p < (len(puntos) - 1) else 0
            distancias.append(math.sqrt(abs(puntos[comparcion][1] - puntos[p][1]) ** 2 + abs(puntos[comparcion][0] - puntos[p][0]) ** 2))

        if (distancias[0] + distancias[1] != distancias[2]) and (distancias[1] + distancias[2] != distancias[0]) and (distancias[2] + distancias[0] != distancias[1]):
            if not 0 in distancias:
                if self.tripletaPitagorica(distancias[0], distancias[1], distancias[2]):
                    res = 'es un triangulo rectangulo'
                elif distancias[0] == distancias[1] or distancias[1] == distancias[2] or distancias[2] == distancias[0]:
                    if distancias[0] == distancias[1] and distancias[1] == distancias[2]:
                        res = 'es un triangulo equilatero'
                    else:
                        res = 'es un triangulo isoseles'
                else:
                    res = 'es un triangulo escaleno'
            else:
                res = 'esto no es un triangulo'
        else:
            res = 'esto no es un triangulo'
        return res


res = Resolve()
ciclo = True
while(ciclo):
    print '\n'
    print '0.   SALIR'
    print '1.   saber si un numero es par'
    print '2.   conversion de F a C'
    print '3.   potencia'
    print '4.   hilera'
    print '6.   lista ordenable'
    print '7.   lista con multiplos de 4 o 7, menores a 1000'
    print '8.   triangulo'
    print '9.   tripleta pitagorica'
    print '10.  Tuplas y triangulos'
    print '11.  palindromo'
    print '12.  Leer un numero'

    print '\n'
    opcion = raw_input('Elige una opcion: ')
    print '\n'

    if opcion == '1':
        print '---- Numero par ----'
        aux = input('Ingersa un numero: ')
        print '%s es par?' % (aux)
        print 'Si lo es' if res.par(aux) else 'No lo es'
    elif opcion == '2':
        print '---- Equivalencia de temperatura ----'
        aux = input('Ingersa la temeratura en Farenheits: ')
        print '---- Equivalencia de temperatura ----'
        print '%sF es equivalente a %sC' % (aux, res.celcios(aux))
    elif opcion == '3':
        print '---- Potencia ----'
        base = input('ingresa la base de la potencia: ')
        exp = input('ingresa el exponente de la potencia: ')
        print 'El resultado es: %s' % (res.pow(base, exp))
    elif opcion == '4':
        print '---- Hilera ----'
        hilera = raw_input('Escribe la hilera: ')
        longitud = input('Escribe la logitud del parrafo: ')
        print res.hilera(hilera, longitud)
    elif opcion == '6':
        print '---- Bubble Sort ----'
        elem = []
        for i in range(50):
            elem.append(res.aleatorio(100))
        print 'original: %s' % (elem)
        print 'ordenada: %s' % (res.bubbleSort(elem, False))
    elif opcion == '7':
        print '---- Multiplos de 4 o 7 menores que 1000 ----'
        elem = []
        for i in range(50):
            elem.append(res.aleatorio(1500))
        print 'lista: %s' % (elem)
        print '\n'
        print 'resultado: %s' % (res.multiplos(elem))
    elif opcion == '8':
        print '---- Triangulo ----'
        aux = input('Ingresa el alto del triangulo: ')
        print '\n'
        print res.piramide(aux)
    elif opcion == '9':
        print '---- Tripleta Pitagorica ----'
        num1 = input('primer numero: ')
        num2 = input('segundo numero: ')
        num3 = input('tercer numero: ')
        print 'si son lados de un triangulo rectangulo :)' if res.tripletaPitagorica(num1, num2, num3) else 'no son lados de un triangulo rectangulo :('
    elif opcion == '10':
        tup1 = (0,0)
        tup2 = (2, 0)
        tup3 = (2,2)
        print 'tuplas %s %s %s' % ()
        print res.validacionTriangulo(tup1, tup2, tup3)
    elif opcion == '11':
        print '---- Palindromo ----'
        pal = raw_input('Escribe una plabra ')
        print 'Es palindromo!' if res.palindromo(pal) else 'No es palindromo :('
    elif opcion == '12':
        num = raw_input('Escribe un numero: ')
        res.leerNumero(num)
    elif opcion == '0':
        ciclo = False


    print '\n'
    sep = raw_input('Preciona enter para continuar... ')
    print '\n'

