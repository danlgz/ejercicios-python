import random

class Numero(object):
    def __init__(self, max):
        self.max = max

    def obtenerAleatorio(self):
        return random.randrange(self.max)

numOb = Numero(10)
n1 = numOb.obtenerAleatorio()
n2 = numOb.obtenerAleatorio()

print n1
print n2

if (n1%n2 == 0 and n1 != n2):
    print 'los numeros son divisibles'
elif (n1 > n2):
    print 'el primero es mayor que el segundo'
elif (n1 < n2):
    print 'el segundo es mayot que el tercero'
else:
    print 'los numeros son iguales'
        